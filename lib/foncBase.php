<?php

function choixAlert($message)
{
  $alert = array();
  switch($message)
  {
    case 'query' :
      $alert['messageAlert'] = ERREUR_QUERY;
      break;
    case 'url_non_valide' :
      $alert['messageAlert'] = TEXTE_PAGE_404;
      break;
    case 'identifiant_inconnu' :
      $alert['messageAlert'] = USER_NOT_FOUND;
      break;
    case 'password_error' :
      $alert['messageAlert'] = PSWD_ERROR;
    default :
      $alert['messageAlert'] = MESSAGE_ERREUR;
  }
  return $alert;
}