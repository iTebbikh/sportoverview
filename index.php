<?php
//initialisation des paramètres du site
require_once('./configuration/config.php');
require_once(PATH_LIB.'foncBase.php');
require_once(PATH_TEXTS.LANG.'.php');
require_once(PATH_CONTROLLERS.'index.php');

//vérification de la page demandée 
if(isset($_GET['page']))
{
  $page = htmlspecialchars($_GET['page']);
  if(!is_file(PATH_CONTROLLERS.$_GET['page'].".php"))
  { 
    $page = '404'; //page demandée inexistante
  }
}
else
  $page='accueil'; //page d'accueil du site


//appel du controleur
require_once(PATH_CONTROLLERS.$page.'.php'); 

?>