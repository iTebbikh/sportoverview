<!-- haut de lapage -->
<!DOCTYPE html>
<html>
	<head>
    <title><?= TITRE ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="Language" content="<?= LANG ?>"/>

    <link href="<?= PATH_CSS ?>header.css" rel="stylesheet">
    <link href="<?= PATH_CSS ?>competition.css" rel="stylesheet">
    <link href="<?= PATH_CSS ?>methodes_sidebar.css" rel="stylesheet">
    
    <script type="text/javascript" src="<?= PATH_SCRIPTS ?>total_but.js"></script>
    <script type="text/javascript" src="<?= PATH_SCRIPTS ?>methodes_sidebar.js"></script>
  </head> 
  <body>

    <?php  //en-tête
    require_once(PATH_VIEWS.'header.php');
    ?>
    <?php  //sidebar
    require_once(PATH_VIEWS.'methodes_sidebar.php');
    ?>

    <div class="corp">

      
      <div class="display">
        <!-- Selection de la journée(round) du championnat -->
        <select name="methode" id="methode" class="journees">
          <option value="1">Moins de 0.5</option>
          <option value="2">Moins de 1.5</option>
          <option value="3">Moins de 2.5</option>
          <option value="4">Moins de 3.5</option>
          <option value="5">Moins de 4.5</option>
          <option value="6">Moins de 5.5</option>
          <option value="7">Moins de 6.5</option>
          <option value="1">Plus de 0.5</option>
          <option value="2">Plus de 1.5</option>
          <option value="3">Plus de 2.5</option>
          <option value="4">Plus de 3.5</option>
          <option value="5">Plus de 4.5</option>
          <option value="6">Plus de 5.5</option>
          <option value="7">Plus de 6.5</option>
        </select>

        <!-- Tableau d'affichage des matchs -->
        <table id="calendrierMatch">
        </table>
      </div>

    </div>
  </body>

</html>