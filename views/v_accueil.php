<!-- haut de lapage -->
<!DOCTYPE html>
<html>
	<head>
    <title><?= TITRE ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="Language" content="<?= LANG ?>"/>

    <link href="<?= PATH_CSS ?>header.css" rel="stylesheet">
    <link href="<?= PATH_CSS ?>accueil.css" rel="stylesheet">
    <link href="<?= PATH_CSS ?>sidebar.css" rel="stylesheet">
    
    <script type="text/javascript" src="<?= PATH_SCRIPTS ?>accueil.js"></script>
    <script type="text/javascript" src="<?= PATH_SCRIPTS ?>sidebar.js"></script>
  </head> 
  <body>

    <?php  //en-tête
    require_once(PATH_VIEWS.'header.php');
    ?>
    <?php  //sidebar
    require_once(PATH_VIEWS.'sidebar.php');
    ?>

    <div class="corp">
      
      <div class="tableDisplay">
        <!-- Input Date -->
        <table class="tableDate">
          <tr class="date">
            <td> 
              <input type="date" id="date" name="date" value="2020-10-23">
            </td>
          </tr>
        </table>
        
        <!-- Tableau d'affichage des matchs -->
        <table id="calendrierMatch">
        </table>
      </div>

    </div>
  </body>

</html>