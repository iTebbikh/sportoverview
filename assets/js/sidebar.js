//Animation de la sidebar
function sidebarEffects(){
    var acc = document.getElementsByClassName("accordion");
    var i;
    
    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
          panel.style.maxHeight = null;
        } else {
          panel.style.maxHeight = panel.scrollHeight + "px";
        } 
      });
    }
}

//Affichage des compétitions par pays
async function showCompetitions(f){
    var comp = await f.then(function(resultat) {
        if (resultat.ok) { return resultat.json()}
        else {throw ("Error " + resultat.status);}
    }).then(function(competitions) {
        var paysArray = [];

        for (var i = 0; i < competitions.length; i++) {
            var paysCompetition = competitions[i]["paysCompetition"];

            if(!paysArray.includes(paysCompetition)){
                paysArray.push(paysCompetition);

                var button = document.createElement("button");
                button.className = "accordion";
                button.textContent = competitions[i]["paysCompetition"];
                document.getElementById("sidebar").appendChild(button);

                var div = document.createElement("div");
                div.className = "panel";
                for(var j = 0; j < competitions.length; j++) {
                    if(competitions[j]["paysCompetition"] === paysCompetition){
                        var a = document.createElement("a");
                        a.textContent=competitions[j]["nomCompetition"];
                        a.style.fontSize= "14pt";
                        a.style.fontWeight= "bold";
                        a.style.color= "rgb(22, 76, 146)";
                        a.href = "?page=competition&competition="+ competitions[j]["idCompetition"];
                        div.appendChild(a);
                    }
                }
                document.getElementById("sidebar").appendChild(div);
            }

        }
    }).catch(function(err) {
        console.log("Fixtures Promise Error");
    });
    
    sidebarEffects();
    
}

//Lancement de la Récupération
function initialiseSidebar(){
    var f = fetch("http://localhost/SportOverviewAPI/?url=allCompetition");
    showCompetitions(f);
}

window.addEventListener("load", initialiseSidebar);