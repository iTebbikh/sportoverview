// Récupère le nom d'une équipe grâce à son id
async function getTeamNameById(fixture, team){
    var teamFetch = fetch("http://localhost/SportOverviewAPI/?url=getEquipeId/"+fixture[team]);
    var teamName = await teamFetch.then( function(resultat) {
        if (resultat.ok) { return resultat.json()} 
        else {throw ("Error " + resultat.status);}
    }).then(function(team) {
        return team[0]["nom"];
    }).catch(function(err) {
        console.log("Team Promise Error");
    });
    return teamName;
}

// Récupère la liste des Scores des matchs, seul les matchs terminés sont stockés dans la BDD
async function getScoreByMatchId(fixture){
    var teamFetch = fetch("http://localhost/SportOverviewAPI/?url=getScoreId/"+fixture["idMatch"]);
    let scoreTeam = await teamFetch.then( function(resultat) {
        if (resultat.ok) { return resultat.json()} 
        else {throw ("Error " + resultat.status);}
    }).then(function(score) {
        return score[0];
    }).catch(function(err) {
        console.log("Score Promise Error");
    });
    return scoreTeam;
}

// Affiche la liste des matchs
async function showFixtures(f, roundLastMatch){

    // Récupère la liste des match
    f.then(function(resultat) {
        if (resultat.ok) { return resultat.json()}
        else {throw ("Error " + resultat.status);}
    }).then(async function(fixtures) {
        for (var i = 0; i < fixtures.length; i++) {
            // Vérifie si la journée d'un match correspond à celle sélectionnée dans la combobox
            if(fixtures[i]["roundMatch"] == roundLastMatch){
                // Parsage de la date à afficher
                var dateMatch = fixtures[i]["dateMatch"];
                var dateFormat = new Date(dateMatch);
                var dayInt = dateFormat.getDay();
                var day;
                switch (dayInt) {
                    case 0:
                        day = "Lun";
                        break;
                    case 1:
                        day = "Mar";
                        break;
                    case 2:
                        day = "Mer";
                        break;
                    case 3:
                        day = "Jeu";
                        break;
                    case 4:
                        day = "Ven";
                        break;
                    case 5:
                        day = "Sam";
                        break;
                    case 6:
                        day = "Dim";
                        break;
                }

                var monthInt = dateFormat.getMonth();
                var month;
                switch (monthInt) {
                    case 0:
                        month = "Jan";
                        break;
                    case 1:
                        month = "Fev";
                        break;
                    case 2:
                        month = "Mar";
                        break;
                    case 3:
                        month = "Avr";
                        break;
                    case 4:
                        month = "Mai";
                        break;
                    case 5:
                        month = "Jun";
                        break;
                    case 6:
                        month = "Jui";
                        break;
                    case 7:
                        month = "Aou";
                        break;
                    case 8:
                        month = "Sep";
                        break;
                    case 9:
                        month = "Oct";
                        break;
                    case 10:
                        month = "Nov";
                        break;
                    case 11:
                        month = "Dec";
                        break;
                }
                var dateformated = day + " " + dateFormat.getDate() + " " + month + " " + dateFormat.getFullYear() +" "+ dateFormat.getHours() + ":" + (dateFormat.getMinutes()<10?'0':'') + dateFormat.getMinutes();
            
                //Création de la ligne à ajouter au tableau des matchs
                var tr = document.createElement("tr");
                tr.className="fixtureRow";

                var fixtureRow = "<td style='font-size:12pt; font-weight:bold; width:200px; text-align:center;'>"+dateformated+"</td> <td style='width: 80px;'><img src='https://media.api-sports.io/football/teams/"+fixtures[i]["idEquipeDomicileMatch"]+".png'/></td>";

                var homeTeamName = await getTeamNameById(fixtures[i], "idEquipeDomicileMatch");
                fixtureRow += "<td style='font-size:14pt;width: 250px;'>"+homeTeamName+"</td>";

                //Tous les matchs de l'année sont récupérées, mais seule ceux étant terminés ont un score, ce qui génère une erreur
                try{
                    var scoreArray = await getScoreByMatchId(fixtures[i]);
                    fixtureRow += "<td style='font-size:14pt;'>"+scoreArray["butDomicileScore"]+"</td> <td style='font-size:14pt;'> - </td> <td style='font-size:14pt;'>"+scoreArray["butExterieurScore"]+"</td>";
                } catch {
                    fixtureRow += "<td> </td> <td> - </td> <td> </td>";
                }

                var awayTeamName = await getTeamNameById(fixtures[i], "idEquipeExterieurMatch");
                fixtureRow += "<td style='text-align: right;font-size:14pt;width: 250px;'>"+awayTeamName+"</td>";

                fixtureRow += "<td style='text-align: right;width: 80px;'><img src='https://media.api-sports.io/football/teams/"+fixtures[i]["idEquipeExterieurMatch"]+".png' /></td>";
                
                tr.innerHTML = await fixtureRow;
                
                document.getElementById("calendrierMatch").appendChild(tr);
            }
        }
        //console.log("Fixtures Promise Success");
    }).catch(function(err) {
        console.log("Fixtures Promise Error");
    });
    
}

async function lancement(){
    // Récupération de l'id compétition
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const competition = urlParams.get('competition');

    // Récupération du numéro de la journée en cours
    var roundLastMatch = await fetch("http://localhost/SportOverviewAPI/?url=getLastMatchIdCompetition/"+competition).then(function(resultat) {
        if (resultat.ok) { return resultat.json()}
        else {throw ("Error " + resultat.status);}
    }).then(function(fixture) {
        return fixture[0]["roundMatch"];
    }).catch(function(err) {
        console.log("Last Fixtures Promise Error");
    });

    //Remplissage de la combobox avec le nombre de journée de la saison d'une compétition
    var f = fetch("http://localhost/SportOverviewAPI/?url=getMatchIdCompetition/"+competition);
    f.then(function(resultat) {
        if (resultat.ok) { return resultat.json()}
        else {throw ("Error " + resultat.status);}
    }).then(async function(fixtures) {
        var nbMatch = 0
        for (var i = 0; i < fixtures.length; i++) {
            if(fixtures[i]["roundMatch"]>nbMatch){
                nbMatch = fixtures[i]["roundMatch"];
            }
        }
        for (var i = 1; i <= nbMatch; i++) {
            var option = document.createElement("option");
            option.value=i;
            option.textContent="Journee "+i;
            if(i == roundLastMatch){
                option.selected=true;
            }
            document.getElementById("journees").appendChild(option);
        }
    }).catch(function(err) {
        console.log("Fixtures Promise Error");
    });

    //Récupération de l'affichage
    var f = fetch("http://localhost/SportOverviewAPI/?url=getMatchIdCompetition/"+competition);
    showFixtures(f, roundLastMatch);

    //Appeler à chaque changement de la combobox
    document.getElementById("journees").addEventListener("change", function() {
        var journee = this.value;
        document.getElementById("calendrierMatch").innerHTML="";
        var f = fetch("http://localhost/SportOverviewAPI/?url=getMatchIdCompetition/"+competition);
        showFixtures(f, journee);
    });
}

window.addEventListener("load", lancement);
