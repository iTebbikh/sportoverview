// Récupère le nom d'une équipe grâce à son id
async function getTeamNameById(fixture, team){
    var teamFetch = fetch("http://localhost/SportOverviewAPI/?url=getEquipeId/"+fixture[team]);
    var teamName = await teamFetch.then( function(resultat) {
        if (resultat.ok) { return resultat.json()} 
        else {throw ("Error " + resultat.status);}
    }).then(function(team) {
        return team[0]["nom"];
    }).catch(function(err) {
        console.log("Team Promise Error");
    });
    return teamName;
}

// Récupère la liste des Scores des matchs, seul les matchs terminés sont stockés dans la BDD
async function getScoreByMatchId(fixture){
    var teamFetch = fetch("http://localhost/SportOverviewAPI/?url=getScoreId/"+fixture["idMatch"]);
    let scoreTeam = await teamFetch.then( function(resultat) {
        if (resultat.ok) { return resultat.json()} 
        else {throw ("Error " + resultat.status);}
    }).then(function(score) {
        return score[0];
    }).catch(function(err) {
        console.log("Score Promise Error");
    });
    return scoreTeam;
}

// Affiche la liste des matchs
async function showFixtures(f, date){

    // Récupère la liste des match
    f.then(function(resultat) {
        if (resultat.ok) { return resultat.json()}
        else {throw ("Error " + resultat.status);}
    }).then(async function(fixtures) {
        for (var i = 0; i < fixtures.length; i++) {
            // Parsage de la date à afficher
            var dateMatch = fixtures[i]["dateMatch"];
            var dateFormat = new Date(dateMatch);
            var dateformated = dateFormat.getHours() + ":" + (dateFormat.getMinutes()<10?'0':'') + dateFormat.getMinutes();

            //Vérifie si un match correspond à la date de l'input
            var found = dateMatch.match(date);
            if(found !== null){

                //Création de la ligne à ajouter au tableau des matchs
                var tr = document.createElement("tr");
                tr.className="fixtureRow";

                var fixtureRow = "<td style='font-size:14pt; width:70px; text-align:center;'>"+dateformated+"</td> <td style='width: 80px;'><img src='https://media.api-sports.io/football/teams/"+fixtures[i]["idEquipeDomicileMatch"]+".png'/></td>";

                var homeTeamName = await getTeamNameById(fixtures[i], "idEquipeDomicileMatch");
                fixtureRow += "<td style='font-size:14pt;width: 250px;'>"+homeTeamName+"</td>";

                //Tous les matchs de l'année sont récupérées, mais seule ceux étant terminés ont un score, ce qui génère une erreur
                try{
                    var scoreArray = await getScoreByMatchId(fixtures[i]);
                    fixtureRow += "<td style='font-size:14pt;'>"+scoreArray["butDomicileScore"]+"</td> <td style='font-size:14pt;'> - </td> <td style='font-size:14pt;'>"+scoreArray["butExterieurScore"]+"</td>";
                } catch {
                    fixtureRow += "<td> </td> <td> - </td> <td> </td>";
                }

                var awayTeamName = await getTeamNameById(fixtures[i], "idEquipeExterieurMatch");
                fixtureRow += "<td style='text-align: right;font-size:14pt;width: 250px;'>"+awayTeamName+"</td>";

                fixtureRow += "<td style='text-align: right;width: 80px;'><img src='https://media.api-sports.io/football/teams/"+fixtures[i]["idEquipeExterieurMatch"]+".png' /></td>";
                
                tr.innerHTML = await fixtureRow;
                
                document.getElementById("calendrierMatch").appendChild(tr);
            }
        }
        //console.log("Fixtures Promise Success");
    }).catch(function(err) {
        console.log("Fixtures Promise Error");
    });
    
}

function lancement(){
    //Affecte à l'input la date du jour
    Date.prototype.toDateInputValue = (function() {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });
    document.getElementById("date").value = new Date().toDateInputValue();

    //Récupération de l'affichage
    var f = fetch("http://localhost/SportOverviewAPI/?url=allMatch");
    var date = document.getElementById("date").value;
    showFixtures(f, date);

    //Appeler à chaque changement de l'input date
    document.getElementById("date").addEventListener("change", function() {
        var date = this.value;
        document.getElementById("calendrierMatch").innerHTML="";
        var f = fetch("http://localhost/SportOverviewAPI/?url=allMatch");
        showFixtures(f, date);
    });
}

window.addEventListener("load", lancement);
