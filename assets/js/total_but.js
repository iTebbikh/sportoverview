// Récupère le nom d'une équipe grâce à son id
async function getTeamNameById(fixture, team){
    var teamFetch = fetch("http://localhost/SportOverviewAPI/?url=getEquipeId/"+fixture[team]);
    var teamName = await teamFetch.then( function(resultat) {
        if (resultat.ok) { return resultat.json()} 
        else {throw ("Error " + resultat.status);}
    }).then(function(team) {
        return team[0]["nom"];
    }).catch(function(err) {
        console.log("Team Promise Error");
    });
    return teamName;
}

// Récupère la liste des Scores des matchs, seul les matchs terminés sont stockés dans la BDD
async function getScoreByMatchId(fixture){
    var teamFetch = fetch("http://localhost/SportOverviewAPI/?url=getScoreId/"+fixture["idMatch"]);
    let scoreTeam = await teamFetch.then( function(resultat) {
        if (resultat.ok) { return resultat.json()} 
        else {throw ("Error " + resultat.status);}
    }).then(function(score) {
        return score[0];
    }).catch(function(err) {
        console.log("Score Promise Error");
    });
    return scoreTeam;
}

// Affiche la liste des matchs
async function showFixtures(f, roundLastMatch){

    // Récupère la liste des match
    f.then(function(resultat) {
        if (resultat.ok) { return resultat.json()}
        else {throw ("Error " + resultat.status);}
    }).then(async function(fixtures) {
        for (var i = 0; i < fixtures.length; i++) {
            // Parsage de la date à afficher
            var dateMatch = fixtures[i]["dateMatch"];
            var dateFormat = new Date(dateMatch);
            var dayInt = dateFormat.getDay();
            var day;
            switch (dayInt) {
                case 0:
                    day = "Lun";
                    break;
                case 1:
                    day = "Mar";
                    break;
                case 2:
                    day = "Mer";
                    break;
                case 3:
                    day = "Jeu";
                    break;
                case 4:
                    day = "Ven";
                    break;
                case 5:
                    day = "Sam";
                    break;
                case 6:
                    day = "Dim";
                    break;
            }

            var monthInt = dateFormat.getMonth();
            var month;
            switch (monthInt) {
                case 0:
                    month = "Jan";
                    break;
                case 1:
                    month = "Fev";
                    break;
                case 2:
                    month = "Mar";
                    break;
                case 3:
                    month = "Avr";
                    break;
                case 4:
                    month = "Mai";
                    break;
                case 5:
                    month = "Jun";
                    break;
                case 6:
                    month = "Jui";
                    break;
                case 7:
                    month = "Aou";
                    break;
                case 8:
                    month = "Sep";
                    break;
                case 9:
                    month = "Oct";
                    break;
                case 10:
                    month = "Nov";
                    break;
                case 11:
                    month = "Dec";
                    break;
            }
            var dateformated = day + " " + dateFormat.getDate() + " " + month + " " + dateFormat.getFullYear() +" "+ dateFormat.getHours() + ":" + (dateFormat.getMinutes()<10?'0':'') + dateFormat.getMinutes();
        
            //Création de la ligne à ajouter au tableau des matchs
            var tr = document.createElement("tr");
            tr.className="fixtureRow";

            var fixtureRow = "<td style='font-size:12pt; font-weight:bold; width:200px; text-align:center;'>"+dateformated+"</td> <td style='width: 80px;'><img src='https://media.api-sports.io/football/teams/"+fixtures[i]["idEquipeDomicileMatch"]+".png'/></td>";

            var homeTeamName = await getTeamNameById(fixtures[i], "idEquipeDomicileMatch");
            fixtureRow += "<td style='font-size:14pt;width: 250px;'>"+homeTeamName+"</td>";

            //Tous les matchs de l'année sont récupérées, mais seule ceux étant terminés ont un score, ce qui génère une erreur
            try{
                var scoreArray = await getScoreByMatchId(fixtures[i]);
                fixtureRow += "<td style='font-size:14pt;'>"+scoreArray["butDomicileScore"]+"</td> <td style='font-size:14pt;'> - </td> <td style='font-size:14pt;'>"+scoreArray["butExterieurScore"]+"</td>";
            } catch {
                fixtureRow += "<td> </td> <td> - </td> <td> </td>";
            }

            var awayTeamName = await getTeamNameById(fixtures[i], "idEquipeExterieurMatch");
            fixtureRow += "<td style='text-align: right;font-size:14pt;width: 250px;'>"+awayTeamName+"</td>";

            fixtureRow += "<td style='text-align: right;width: 80px;'><img src='https://media.api-sports.io/football/teams/"+fixtures[i]["idEquipeExterieurMatch"]+".png' /></td>";
            
            tr.innerHTML = await fixtureRow;
            
            document.getElementById("calendrierMatch").appendChild(tr);
        }
        //console.log("Fixtures Promise Success");
    }).catch(function(err) {
        console.log("Fixtures Promise Error");
    });
    
}

async function lancement(){
    // Récupération de la valeur de la methode
    var methode = document.getElementById("methode").value;

    //Récupération de l'affichage
    var f = fetch("http://localhost/SportOverviewAPI/?url=totalBut/"+methode);
    showFixtures(f, methode);

    //Appeler à chaque changement de la combobox
    document.getElementById("methode").addEventListener("change", function() {
        var methode = this.value;
        document.getElementById("calendrierMatch").innerHTML="";
        console.log("")
        var f = fetch("http://localhost/SportOverviewAPI/?url=totalBut/"+methode);
        showFixtures(f, methode);
    });
}

window.addEventListener("load", lancement);
