<?php

//Langue du site
const LANG = 'FR-fr';

//dossiers racines du site
define('PATH_CONTROLLERS', './controllers/c_');
define('PATH_VIEWS', './views/v_');
define('PATH_LIB', './lib/');
define('PATH_ASSETS', './assets/');
define('PATH_TEXTS','./languages/');

//sous-dossiers
define('PATH_CSS', PATH_ASSETS.'css/');
define('PATH_IMAGES', PATH_ASSETS.'images/');
define('PATH_SCRIPTS', PATH_ASSETS.'js/');

//fichiers
define('PATH_LOGO', PATH_IMAGES.'logo.png');



?>