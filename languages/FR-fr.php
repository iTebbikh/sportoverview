<?php
//isolement dans des constantes les textes affichés sur le site
define('LOGO', 'Logo SportOverview'); // Affiché si image non trouvée

define('TEXTE_PAGE_404','Ooups, la page demandée n\'existe pas !');
define('MESSAGE_ERREUR',"Une erreur s'est produite.");

define('MENU_FOOT','FOOTBALL');
define('MENU_TENNIS','TENNIS');
define('MENU_BASKET','BASKETBALL');

define('PLANNING','Planning');
define('CLASSEMENT','Classement');
define('BUTEURS','Buteurs');
define('PASSEURS_DECISIFS','Passeurs décisifs');

define('TITRE', 'SportOverview');
define('INFO', 'Information');
define('STATS', 'Statistique');
define('ERREUR_QUERY', 'Problème d\'accès à la base de données.');
